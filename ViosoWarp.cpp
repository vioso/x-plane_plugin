#define IBM 1
#define XPLM300
#define XPLM303
#define XPLM400
#define NOMINMAX

#include <stdio.h>
#include <string.h>

//#include "SDK/CHeaders/XPLM/XPLMScenery.h"
//#include "SDK/CHEADERS/XPLM/XPLMInstance.h"
#include "SDK/CHEADERS/XPLM/XPLMDisplay.h"
#include "SDK/CHEADERS/XPLM/XPLMGraphics.h"
#include "SDK/CHeaders/XPLM/XPLMDataAccess.h"
#include "SDK/CHEADERS/XPLM/XPLMUtilities.h"
#include <GL/gl.h>
#include <memory>
#include <array>
#include <vector>
#include <map>
#include <algorithm>
#include <tchar.h>
#include "VIOSOWarpBlend.hpp"
#include "../VIOSOWarpBlend/mmath.h"
#include "DirectXMath.h"

using namespace std;
extern "C" IMAGE_DOS_HEADER __ImageBase;

static bool						g_bEnabled = false;
static bool						g_modern = false;
static auto s_phase = xplm_Phase_LastCockpit;
//static const auto s_phase = xplm_Phase_Window;

typedef struct Point
{
	int x, y;
} Point;
VWBmap<HWND>						g_warpers;
struct XMonitorInfo {
	int inLeftPx;
	int inTopPx;
	int inRightPx;
	int inBottomPx;
	float aspectRatio;
	RECT rcMonitor;
	HGLRC ctx;
};
std::map< int, XMonitorInfo >   g_monInfo;
vector<XPLMDataRef> 			g_refs;
int								g_activeMonitor = 0;

class XPlaneRef : public IXPlaneRef
{
private:
	long refcnt; // the reference counter
	VWB_pfnXPLMSetGraphicsState XPLMSetGraphicsState;
	VWB_pfnXPLMBindTexture2d XPLMBindTexture2d;
	VWB_pfnXPLMGenerateTextureNumbers XPLMGenerateTextureNumbers;

	// hide constructors
	XPlaneRef() = default;
	XPlaneRef( XPlaneRef const& other ) = default;

public:
	virtual HRESULT STDMETHODCALLTYPE QueryInterface(
		/* [in] */ REFIID riid,
		/* [iid_is][out] */ _COM_Outptr_ void __RPC_FAR* __RPC_FAR* ppvObject );
	virtual ULONG __stdcall AddRef();
	virtual ULONG __stdcall Release();

	virtual void __stdcall SetGraphicsState( int inEnableFog, int inNumberTexUnits, int inEnableLighting, int inEnableAlphaTesting, int inEnableAlphaBlending, int inEnableDepthTesting, int inEnableDepthWriting );
	virtual void __stdcall BindTexture2d( int inTextureNum, int inTextureUnit );
	virtual void __stdcall GenerateTextureNumbers( int* outTextureIDs, int  inCount );

	static HRESULT CreateInstance( REFIID riid, VWB_pfnXPLMSetGraphicsState fnSGS, VWB_pfnXPLMBindTexture2d fnBT2D, VWB_pfnXPLMGenerateTextureNumbers fnGTN, void** ppVObject );
};


HRESULT XPlaneRef::QueryInterface( REFIID riid, void** ppvObject )
{
	if( nullptr == ppvObject )
		return E_INVALIDARG;
	if( __uuidof( IUnknown ) == riid ||
		__uuidof( IXPlaneRef ) == riid )
	{
		*ppvObject = this;
		AddRef();
		return S_OK;
	}
	return  E_FAIL;
}
ULONG XPlaneRef::AddRef() { return ++refcnt; };
ULONG XPlaneRef::Release() {
	if( !--refcnt ) {
		delete this;
	}
	return refcnt;
}


void XPlaneRef::SetGraphicsState( int inEnableFog, int inNumberTexUnits, int inEnableLighting, int inEnableAlphaTesting, int inEnableAlphaBlending, int inEnableDepthTesting, int inEnableDepthWriting )
{
	XPLMSetGraphicsState( inEnableFog, inNumberTexUnits, inEnableLighting, inEnableAlphaTesting, inEnableAlphaBlending, inEnableDepthTesting, inEnableDepthWriting );
}

void XPlaneRef::BindTexture2d( int inTextureNum, int inTextureUnit )
{
	XPLMBindTexture2d( inTextureNum, inTextureUnit );
}

void XPlaneRef::GenerateTextureNumbers( int* outTextureIDs, int  inCount )
{
	XPLMGenerateTextureNumbers( outTextureIDs, inCount );
}

HRESULT XPlaneRef::CreateInstance( REFIID riid, VWB_pfnXPLMSetGraphicsState fnSGS, VWB_pfnXPLMBindTexture2d fnBT2D, VWB_pfnXPLMGenerateTextureNumbers fnGTN, void** ppVObject )
{
	if( nullptr == ppVObject ||
		nullptr == fnSGS ||
		nullptr == fnBT2D ||
		nullptr == fnGTN )
		return E_INVALIDARG;
	if( __uuidof( IXPlaneRef ) == riid )
	{
		XPlaneRef* pO = new XPlaneRef();
		pO->refcnt = 1;
		pO->XPLMSetGraphicsState = fnSGS;
		pO->XPLMBindTexture2d = fnBT2D;
		pO->XPLMGenerateTextureNumbers = fnGTN;
		*ppVObject = pO;
		return S_OK;
	}
	return E_FAIL;
}

static void my_ReceiveMonitorBoundsOS(
	int                  inMonitorIndex,
	int                  inLeftPx,
	int                  inTopPx,
	int                  inRightPx,
	int                  inBottomPx,
	void* inRefcon )
{
	auto& [ it, wasNew ] = reinterpret_cast< std::map< int, XMonitorInfo >* >( inRefcon )->emplace( inMonitorIndex, XMonitorInfo{ inLeftPx, inBottomPx, inRightPx, inTopPx, float( inRightPx - inLeftPx ) / ( inTopPx - inBottomPx ) } );
	if( !wasNew )
	{
		auto& v = it->second;
		v.inLeftPx = inLeftPx;
		v.inBottomPx = inBottomPx;
		v.inRightPx = inRightPx;
		v.inTopPx = inTopPx;
		v.aspectRatio = float( inRightPx - inLeftPx ) / ( inTopPx - inBottomPx );
	}
}

BOOL my_Monitorenumproc(
	HMONITOR hMon,
	HDC hDC,
	LPRECT pRc,
	LPARAM param
)
{
	auto rc = reinterpret_cast< LPRECT >( param );
	UnionRect( rc, rc, pRc );
	return TRUE;
}

static int my_draw_tex(
					   XPLMDrawingPhase     inPhase,
					   int                  inIsBefore,
					   void *               inRefcon)
{
	if( g_bEnabled && s_phase == inPhase && 0 == inIsBefore )
	{
		HWND hWnd;
		if( g_modern )
			XPLMGetDatavi( g_refs[18], (int*)&hWnd, 0, 2 );
		else
		{
			HDC hDC = ::wglGetCurrentDC();
			hWnd = ::WindowFromDC( hDC );
		}
		//int fbo = XPLMGetDatai( g_refs[19] );
		//int l = XPLMGetDatai( g_refs[20] );
		//int t = XPLMGetDatai( g_refs[21] );
		//HANDLE hT = ::GetCurrentThread();
		//XPLMDataRef ref2 = XPLMFindDataRef( "sim/operation/windows/system_window_64" );

		auto res = g_warpers.try_emplace( hWnd, nullptr );
		auto& ref = res.first->second;
		if( res.second )
		{ // if the context is unknown, we try to estabish a warper for it
			char path[MAX_PATH] = { 0 };
			char chName[MAX_PATH] = "main";

			::GetModuleFileNameA( reinterpret_cast<HMODULE>( &__ImageBase ), path, _countof( path ) );
			if( char* px = ::strrchr( path, _T( '\\' ) ) )
			{
				::strcpy_s( px, _countof( path ) - ( px - path ), "\\VIOSOWarpBlend64.dll" );
				try {
					XPlaneRef* pRef;
					if( SUCCEEDED( XPlaneRef::CreateInstance( __uuidof( IXPlaneRef ), &XPLMSetGraphicsState, &XPLMBindTexture2d, &XPLMGenerateTextureNumbers, (void**)&pRef ) ) )
					{
						RECT r = { 0 };
						::GetWindowRect( hWnd, &r );
						//if( 0 != r.left || 0 != r.top )
						{
							for( auto& mi : g_monInfo )
							{
								if( ::PtInRect( &mi.second.rcMonitor, *(POINT*)&r ) )
								{
									sprintf_s( chName, "Monitor%d", mi.first );
									mi.second.ctx = ::wglGetCurrentContext();
									// find out, if there is another window is using the same monitor, so we add a _X
									// collect all names, that match, either exactly (terminating '\0' comes next) or having a suffix already ( '_' comes next )
									vector<string> names;
									auto n = strlen( chName );
									for( auto const& [wnd, warper] : g_warpers )
									{
										if( warper )
										{
											auto const& name = warper->w.get().channel;
											if( 0 == strncmp( chName, name, n ) && ( '\0' == name[n] || '_' == name[n] ) && wnd != hWnd ) // there is already a reference to the same monitor, which is not me
											{
												names.push_back( name );
											}
										}
									}
									if( !names.empty() )
									{
										sprintf_s( chName + n, ARRAYSIZE( chName ) - n, "_%Iu", names.size() );
									}
									break;
								}
							}
						}
						ref = make_shared<VWBmap<HGLRC>::PtrT>( VWBmap<HGLRC>::BaseT{}, path, pRef, "VIOSOWarpBlend.ini", chName );
						if( VWB_ERROR_NONE != ref->w.Init() )
						{
							char op[100];
							sprintf_s( op, "Vioso Warp plugin: Plugin failed to load channel\"%s\". Init failed.\n", chName );
							XPLMDebugString( op );
							ref.reset();
						}
						else
						{
							char op[100];
							sprintf_s( op, "Vioso Warp plugin: Successfully loaded channel \"%s\".\n", chName );
							XPLMDebugString( op );
						}
						pRef->Release();
					}
					else
					{
						XPLMDebugString( "Vioso Warp plugin: Vioso Warp plugin failed to create XPlaneRef object.\n" );
						ref.reset();
						g_bEnabled = false;
					}
				}
				catch( VWB_ERROR& )
				{
					ref.reset();
					g_bEnabled = false;
					XPLMDebugString( "Vioso Warp plugin: Vioso Warp plugin failed to load. Creating warper failed: dll or ini not found.\n" );
				}
			}
			else
			{
				XPLMDebugString( "Vioso Warp plugin: Vioso Warp plugin failed to load. Base module not found.\n" );
				ref.reset();
				g_bEnabled = false;
			}

		}
		if( ref ) // if creating the warper has failed for some reason, do not try to warp; this is maybe intended, to keep a certain monitor untouched
		{

			ref->w.Render( VWB_UNDEFINED_GL_TEXTURE, VWB_STATEMASK_CLEARBACKBUFFER | VWB_STATEMASK_VIEWPORT );

			// as we do not get any "pre render" call, we set up the next frame's frustum after rendering.
			// This way we're one frame behind the motion, but rendering and warping using same values
			// so the visual does not tear.
			//VWB_VEC3f local_pos(
			//	XPLMGetDataf( g_refs[0] ),
			//	XPLMGetDataf( g_refs[1] ),
			//	XPLMGetDataf( g_refs[2] )
			//);
			//VWB_VEC3f local_rot(
			//	XPLMGetDataf( g_refs[3] ),
			//	XPLMGetDataf( g_refs[4] ),
			//	XPLMGetDataf( g_refs[5] )
			//);
			//VWB_VEC3f pilot_offset(
			//	XPLMGetDataf( g_refs[6] ),
			//	XPLMGetDataf( g_refs[7] ),
			//	XPLMGetDataf( g_refs[8] )
			//);
			VWB_VEC3f dir;
			//(
			//	XPLMGetDataf( g_refs[9] ),
			//	XPLMGetDataf( g_refs[10] ),
			//	XPLMGetDataf( g_refs[11] )
			//);

			float fovH; // = XPLMGetDataf( g_refs[12] );
			float fovV; // = XPLMGetDataf( g_refs[13] );

			static bool signs[3]{ false,false,false };
			float eye[3] = { 0 };
			float rot[3] = { 0 };
			float vpos[3];
			float vdir[3];
			float clip[6];
			if( VWB_ERROR_NONE == ref->w.GetPosDirClip( eye, rot, vpos, vdir, clip, true ) )
			{
				dir = VWB_VEC3f::ptr( vdir ) * RAD2DEG( 1 );
				fovH = 2 * atan( clip[0] / clip[4] ) * RAD2DEG( 1 );
				fovV = 2 * atan( clip[1] / clip[4] ) * RAD2DEG( 1 );
				if( signs[0] )
					XPLMSetDataf( g_refs[9], -dir.x );
				else
					XPLMSetDataf( g_refs[9], dir.x );
				if( signs[1] )
					XPLMSetDataf( g_refs[10], -dir.y );
				else
					XPLMSetDataf( g_refs[10], dir.y );

				if( signs[2] )
					XPLMSetDataf( g_refs[11], -dir.z );
				else
					XPLMSetDataf( g_refs[11], dir.z );

				XPLMSetDataf( g_refs[12], fovH );
				XPLMSetDataf( g_refs[13], fovV );

				//float proj[16]{};
				//float view[16]{};
				//ref->w.GetViewProj( eye, rot, view, proj );
				//float projX[16]{};
				//float viewX[16]{};
				//float viewX2[16]{};
				//XPLMGetDatavf( g_refs[14], projX, 0, 16 );
				//XPLMGetDatavf( g_refs[15], viewX, 0, 16 );
				//XPLMGetDatavf( g_refs[16], viewX2, 0, 16 );
				//// retrofit read matrices
				//ref->w.SetViewProj( viewX2, projX );
			}
		}
	}
	return 0;
}

PLUGIN_API int XPluginStart(char * name, char * sig, char * desc)
{
	XPLMDebugString( "Vioso Warp plugin: Vioso Warp plugin started.\n" );

	strcpy_s( name, 40, "VIOSO Warp & Blend" );
	strcpy_s( sig, 40, "vioso.warp.blend" );
	strcpy_s( desc, 100, "Use warp and blend map to deform screen before swap buffers." );

	if( XPLMDataRef r = XPLMFindDataRef( "sim/graphics/view/using_modern_driver" ) )
	{
		g_modern = 0 != XPLMGetDatai( r );
		if( g_modern ) 
			s_phase = xplm_Phase_Window;
	}

	//XPLMCreateInstance
	const array refNames = {
		"sim/flightmodel/position/local_x", // x
		"sim/flightmodel/position/local_y",  // y 
		"sim/flightmodel/position/local_z",   // z
		"sim/flightmodel/position/theta", // pitch
		"sim/flightmodel/position/phi",  // roll 
		"sim/flightmodel/position/psi",   // yaw
		"sim/aircraft/view/acf_peX", // pilot offset x
		"sim/aircraft/view/acf_peY", // pilot offset y
		"sim/aircraft/view/acf_peZ", // pilot offset z

		"sim/graphics/view/field_of_view_vertical_deg", // pitch offset
		"sim/graphics/view/field_of_view_horizontal_deg", // yaw offset
		"sim/graphics/view/field_of_view_roll_deg", // roll offset
		"sim/graphics/view/field_of_view_deg", // horizontal fov
		"sim/graphics/view/vertical_field_of_view_deg", // vertical fov
		"sim/graphics/view/projection_matrix_3d",
		"sim/graphics/view/modelview_matrix",
		"sim/graphics/view/acf_matrix",
		"sim/operation/windows/system_window",
		"sim/operation/windows/system_window_64",
		"sim/graphics/view/current_gl_fbo",
		"sim/graphics/view/panel_visible_win_l",
		"sim/graphics/view/panel_visible_win_t",
	};
	for( auto& name : refNames )
	{
		g_refs.push_back( XPLMFindDataRef( name ) );
	}
	return 1;
}

PLUGIN_API void XPluginStop(void)
{
	g_warpers.clear();
	XPLMDebugString( "Vioso Warp plugin: Stoped.\n" );
}

PLUGIN_API int XPluginEnable(void)
{
	g_bEnabled = true;
	XPLMDebugString( "Vioso Warp plugin: Enabled.\n" );
	XPLMGetAllMonitorBoundsOS( my_ReceiveMonitorBoundsOS, &g_monInfo );
	RECT rcDesktop{};
	EnumDisplayMonitors( 0, nullptr, my_Monitorenumproc, (LPARAM)&rcDesktop );
	for( auto& [i,mon] : g_monInfo )
	{
		mon.rcMonitor.left = rcDesktop.left + mon.inLeftPx;
		mon.rcMonitor.right = rcDesktop.left + mon.inRightPx;
		mon.rcMonitor.top = rcDesktop.top + mon.inTopPx;
		mon.rcMonitor.bottom = rcDesktop.top + mon.inBottomPx;
	}
	// register callbacks
	XPLMRegisterDrawCallback( my_draw_tex, s_phase, 0, nullptr );
	return 1;
}

PLUGIN_API void XPluginDisable(void)
{
	XPLMDebugString( "Vioso Warp plugin: Disabled.\n" );
	g_bEnabled = false;
	// unregister callback
	XPLMUnregisterDrawCallback( my_draw_tex, s_phase, 0, NULL );
	g_warpers.clear();
	g_monInfo.clear();
}

PLUGIN_API void XPluginReceiveMessage(XPLMPluginID from, long msg, void * p)
{
}

